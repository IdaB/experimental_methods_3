---
title: "Assignment2_Part1_VoiceInSchizophrenia"
author: "Ida Bang Hansen"
date: "July 17, 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Assignment 2 - Part 1 - Assessing voice in schizophrenia

Schizophrenia has been associated with "inappropriate" voice, sometimes monotone, sometimes croaky. A few studies indicate that pitch might be an index of schizophrenia. However, an ongoing meta-analysis of the literature (which you will have a go at in the last assignment) indicates that pitch mean and standard deviation are only weak indicators of diagnosis. Can we do better with our new fancy complex skills?

The corpus you are asked to analyse is a set of voice recordings from people with schizophrenia (just after first diagnosis) and 1-1 matched controls (on gender, age, education). Each participant watched 10 videos of triangles moving across the screen and had to describe them (so you have circa 10 recordings per person). We have already extracted the pitch once every 10 milliseconds and you will have to use this data to assess differences in the voice.

Can you characterize voice in schizophrenia as acoustically different? Report the methods you used to answer this question and the results from the analyses. Add a couple of lines trying to interpret the results (make sense of the difference). E.g. People with schizophrenia tend to have high-pitched voice, and present bigger swings in their prosody than controls. Add a couple of lines describing limitations of the data/analyses if any is relevant.

N.B. There are looots of files to be dealt with. Maybe too many for your computer, depending on how you load the files. This is a challenge for you. Some (complementary) possible strategies:
- You can select a subset of files only (and you have to justify your choice).
- You can learn how to use the apply() or map() functions.
- You can coordinate with classmates.

Hint: There is some information in the filenames that you might need.
Hint: It might be a good idea to first create a function which loads and parses one file, and then loop through the list of files to read them all. For example

```{r}
#libraries
library(purrr) ; library(pacman) ; library(tidyverse) ; library(lmerTest) ; library(FinCal) ; library(lsr)
```

1. In the course of this assignment you have to first select one datafile and figure out how to:
- Extract "standard" descriptors of pitch: Mean, standard deviation, range
- Extract less "standard" descriptors of pitch you can think of (e.g. median, iqr, mean absolute deviation, coefficient of variation)

2. Second you will have to turn the code into a function and loop through all the files (or even better use apply/sapply/lapply)
- Remember to extract the relevant information from the file names (Participant, Diagnosis, Trial, Study)

```{r}
#nice function
read_pitch <- function(filename) {
    d = read.delim(str_c('Pitch/', filename)) # read data
    study <- str_extract(filename, "Study[0-9]") # parse filename
    diagnosis <- str_extract(filename, "D[0-9]")
    subject <- str_extract(filename, "S[10-90]..") 
    trial <- str_extract(filename, "T[0-9].")
    mean_pitch <- mean(d$f0) #extract descriptors
    sd_pitch <- sd(d$f0)
    median_pitch <- median(d$f0)
    range_pitch <- max(d$f0)-min(d$f0)
    min_pitch <- min(d$f0)
    max_pitch <- max(d$f0)
    iqr <- IQR(d$f0)
    CV <- coefficient.variation(sd_pitch, mean_pitch)
    mad <- aad(d$f0) # in package lsr
    data <- data.frame(filename, study, diagnosis, subject, trial, mean_pitch,sd_pitch,median_pitch,range_pitch, min_pitch, max_pitch, iqr, CV, mad) # combine all this data
    return(data)  
}

# test it on just one file while writing the function
#test <- read_pitch("Study1D0S101T10_f0.txt")

# when you've created a function that works, you can
pitch_data <- list.files("Pitch") %>%
    purrr::map_df(read_pitch)
```

```{r}
#CLEAN UP TIME
#make the trial nrs numeric + only contain the numbers
pitch_data$trial <- str_extract_all(pitch_data$trial, "\\-*\\d+\\.*\\d*")
pitch_data$trial <- as.numeric(pitch_data$trial) #make it numeric

#rename diagnosis to just 0 and 1
pitch_data$diagnosis <- str_replace_all(pitch_data$diagnosis, "D0", "0")
pitch_data$diagnosis <- str_replace_all(pitch_data$diagnosis, "D1", "1")
pitch_data$diagnosis <- as.factor(pitch_data$diagnosis) #make it a factor
```

```{r}
#write csv file with the data
write.csv(pitch_data, file = "pitch_data.csv")
```

3. Make one model per acoustic feature and test whether you can observe significant difference due to Diagnosis. Tip: Which other fixed factors should you control for (that is, include in the model)? Which random ones?
```{r}
model_mean <- lmer(mean_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_sd <- lmer(sd_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_median <- lmer(median_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_range <- lmer(range_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_min <- lmer(min_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_max <- lmer(max_pitch ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_iqr <- lmer(iqr ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_CV <- lmer(CV ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)
model_mad <- lmer(mad ~ diagnosis + (1 + trial|subject), data = pitch_data, REML = F)

summary(model_mean) #***
summary(model_sd)
summary(model_median) #***
summary(model_range)
summary(model_min) #***
summary(model_max) #**
summary(model_iqr)
summary(model_CV) #***
summary(model_mad)
```

- Bonus points: cross-validate the model and report the betas and standard errors from all rounds to get an idea of how robust the estimates are.
```{r}
#cross validating the model
```

3a. Is study a significant predictor in these models? What should you infer from this? Does study interact with diagnosis? What should you infer from this?
```{r}
#same models, but with interaction
model_mean2 <- lmer(mean_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_sd2 <- lmer(sd_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_median2 <- lmer(median_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_range2 <- lmer(range_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_min2 <- lmer(min_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_max2 <- lmer(max_pitch ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_iqr2 <- lmer(iqr ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_CV2 <- lmer(CV ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)
model_mad2 <- lmer(mad ~ diagnosis*study + (1 + trial|subject), data = pitch_data, REML = F)

#summary of them all...
```

```{r}
#do they interact?
#MAKE INTERACTION PLOT
```

```{r}
#SOME PLOTS...
#Mean pitch and study
ggplot(pitch_data, aes(x= study, y=mean_pitch, fill = study))+
  geom_bar(stat = "summary", fun.y=mean)+ 
  stat_summary(fun.y=mean)+
  facet_wrap(~diagnosis)+
  geom_errorbar(stat = "summary", fun.data = mean_se, width = 0.2)+ 
  labs(title = "Mean pitch across study and diagnosis", x = "Study", y = "Mean pitch")

#Mean pitch violin plot
ggplot(pitch_data, aes(x=diagnosis, y=mean_pitch, color = diagnosis)) + 
  geom_violin(trim = FALSE)+
  facet_wrap(~study)+
  geom_dotplot(binaxis='y', stackdir='center', dotsize=1, binwidth = 4)+
  labs(title = "Mean pitch across study and diagnosis", x = "Study", y = "Mean pitch")  
  

#Maximum pitch violin plot
ggplot(pitch_data, aes(x=diagnosis, y=max, color = diagnosis)) + 
  geom_violin(trim = FALSE)+
  facet_wrap(~study)+
  geom_dotplot(binaxis='y', stackdir='center', dotsize=1, binwidth = 8)

#Minimum pitch violin plot
ggplot(pitch_data, aes(x=diagnosis, y=min, color = diagnosis)) + 
  geom_violin(trim = FALSE)+
  facet_wrap(~study)+
  geom_dotplot(binaxis='y', stackdir='center', dotsize=1, binwidth = 3)

#Range pitch violin plot
ggplot(pitch_data, aes(x=diagnosis, y=range, color = diagnosis)) + 
  geom_violin(trim = FALSE)+
  facet_wrap(~study)+
  geom_dotplot(binaxis='y', stackdir='center', dotsize=1, binwidth = 5)

pitch_data %>%
  group_by(subject) %>%
  summarize(n())

```



4. Bonus Question: Compare effect size of diagnosis across the different measures. Which measure seems most sensitive?
- Tip: to compare across measures you need to put all of them on the same scale, that is, you need to "standardize" them (z-score)
```{r}
#standardizing and comparing

summary(model_mean)

```

5. Bonus question. In the Clinical Info file you have additional information about the participants. Which additional parameters (e.g. age, gender) should we control for? Report the effects.
```{r}
clinical <- read.delim("Assignment3_VoiceSchizo_DemoData.txt") #read file with extra info

#make new fancy models
```

6. Write a paragraph reporting methods and results

[Next assignment: can we use these measures to build a tool that diagnoses people from voice only?]

## N.B. Remember to save the acoustic features of voice in a separate file, so to be able to load them next time