---
title: "Assignment 3 - Part 2 - Diagnosing Schizophrenia from Voice"
author: "Ida Bang Hansen"
date: "October 17, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Assignment 3 - Diagnosing schizophrenia from voice

In the previous part of the assignment you generated a bunch of "features", that is, of quantitative descriptors of voice in schizophrenia, focusing on pitch.
In the course of this assignment we will use them to try to automatically diagnose schizophrenia from voice only, that is, relying on the set of features you produced last time, we will try to produce an automated classifier.

```{r}
library(tidyverse) ; library(caret) ; library(pROC) ; library(boot)
```

```{r}
pitch_data <- read.csv("pitch_data.csv") #load data created in previous assignment

#rescale data (so the model is able to converge):
#write function to min-max scale
normalize <- function(x)
{
    return((x- min(x)) /(max(x)-min(x)))
}

# To get a vector, use apply instead of lapply
pitch_data_sf <- as.data.frame(apply(pitch_data[,6:14], 2, normalize)) #the scaled features

data_1 <- subset(pitch_data[,1:5]) #info descriptors

pitch_data2 <- bind_cols(data_1, pitch_data_sf) #the shiny, complete, scaled data!
#AT THE END
#remember that a unit change of 1 means the full range of the data! a different scale! either extract the nrs and then calculate it. or make new df with scaling of all the nrs
#+ also translate log odds into probabilities
```

### Question 1: Can you diagnose schizophrenia from pitch range only? If so, how well?
Build a logistic regression to see whether you can diagnose schizophrenia from pitch range only.
```{r}
#the model
model_range <- glmer(diagnosis ~ range_pitch + (1 + trial|subject), pitch_data2, family = "binomial")
summary(model_range) #** p = .00645
```

Calculate the different performance measures (accuracy, sensitivity, specificity, PPV, NPV, ROC curve) on a logistic regression using the full dataset. Don't forget the random effects!
```{r}
#create confusion matrix
pitch_data2$PredictionsPerc <- inv.logit(predict(model_range))
pitch_data2$Predictions[pitch_data2$PredictionsPerc > 0.5] = "1"
pitch_data2$Predictions[pitch_data2$PredictionsPerc <= 0.5] = "0"

#make them factors
pitch_data2$diagnosis <- as.factor(pitch_data2$diagnosis)
pitch_data2$Predictions <- as.factor(pitch_data2$Predictions)

#MATRIX TIME!
c_matrix <- confusionMatrix(data = pitch_data2$Predictions, reference = pitch_data2$diagnosis, positive = "1")
c_matrix
#this gives us accuracy, sensitivity, specificity, PPV, NPV
#ROC curve:
rocCurve <- roc(response = pitch_data2$diagnosis, predictor = pitch_data2$PredictionsPerc)
auc(rocCurve) #0.678
ci (rocCurve) #0.6497 - 0.7062
plot(rocCurve, legacy.axes = TRUE)
```

Then cross-validate the logistic regression and re-calculate performance on the testing folds.
N.B. The cross-validation functions you already have should be tweaked: you need to calculate these new performance measures.
N.B. the predict() function generates log odds (the full scale between minus and plus infinity). Log odds > 0 indicates a choice of 1, below a choice of 0.
N.B. you need to decide whether to calculate performance on each single test fold or save all the prediction for test folds in one dataset, so to calculate overall performance.
N.B. Now you have two levels of structure: subject and study. Should this impact your cross-validation?
```{r}
#Removing "S" from subject column 
pitch_data2$subject <- gsub( "S", "", as.character(pitch_data2$subject), n)
pitch_data2$subject <- as.numeric()

#Right now, some subjects have the same name/subject id, but different diagnoses. This is because the subjects were paired up - therefore, we can hopefully capture some of their shared variance in the random effects... (but maybe we now need to give them all different nrs (as character -> as numeric, so we can run the loop????))
  
#Removing "S" from subject column, making it numeric
pitch_data2$subject <- gsub( "S", "", as.character(pitch_data2$subject), n)
pitch_data2$subject = as.numeric(as.factor(pitch_data2$subject))

#TIME TO CROSS-VALIDATE!!!
#Create folds by subject
Folds <- createFolds(unique(pitch_data$subject), k = 5)
class(pitch_data2)
```

```{r}
#loop de doop - cross-validating range model
N = 1 
pred_perc_train = NULL 
pred_perc_test = NULL
preds_train = NULL
preds_test = NULL
sens_train = NULL
spec_train = NULL
pos_train = NULL
neg_train = NULL
sens_test = NULL
spec_test = NULL
pos_test = NULL
neg_test = NULL
acc_train = NULL
acc_test = NULL

#Loop through each fold 
for (f in Folds) { 
  data_train = filter(pitch_data2, !subject %in% f)
  data_test = filter(pitch_data2, subject %in% f)

  model_1 <- glmer(diagnosis ~ range_pitch + (1 + trial|subject), data_train, family = "binomial")
  
  pred_perc_train <- inv.logit(predict(model_1))
  preds_train <- ifelse(pred_perc_train >= 0.5,1,0)
  preds_train <- as.factor(preds_train)
  
  c_matrix_train <- confusionMatrix(data = preds_train, reference = data_train$diagnosis, positive = '1')
  
  sens_train[N] <- sensitivity(data = preds_train, reference = data_train$diagnosis, positive = "1")
  spec_train[N] <- specificity(data = preds_train, reference = data_train$diagnosis, negative = "0")
  pos_train[N] <- posPredValue(data = preds_train, reference = data_train$diagnosis, positive = "1")
  neg_train [N] <- negPredValue(data = preds_train, reference = data_train$diagnosis, negative = "0")
  acc_train[N] = c_matrix_train$byClass['Balanced Accuracy']

  pred_perc_test <- inv.logit(predict(model_1, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_test[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_test[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  pos_test[N] <- posPredValue(data = preds_test, reference = data_test$diagnosis, positive = "1")
  neg_test[N] <- negPredValue(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_test[N] = c_matrix_test$byClass['Balanced Accuracy']

  N = N + 1
  }
```

### Question 2 - Which single acoustic predictor is the best predictor of diagnosis?
```{r}
#AND NOW IT'S TIME FOR A RIDICULOUSLY LONG LOOP. FOR EACH ACOUSTIC FEATURE
#Prepare loop
N = 1 
pred_perc_test = NULL
preds_test = NULL
sens_2 = NULL
sens_3 = NULL
sens_4 = NULL
sens_5 = NULL
sens_6 = NULL
sens_7 = NULL
sens_8 = NULL
sens_9 = NULL
spec_2 = NULL
spec_3 = NULL
spec_4 = NULL
spec_5 = NULL
spec_6 = NULL
spec_7 = NULL
spec_8 = NULL
spec_9 = NULL
acc_2 = NULL
acc_3 = NULL
acc_4 = NULL
acc_5 = NULL
acc_6 = NULL
acc_7 = NULL
acc_8 = NULL
acc_9 = NULL

#Loop through each fold 
for (f in Folds) { 
  data_train = filter(pitch_data2, !subject %in% f)
  data_test = filter(pitch_data2, subject %in% f)

  model_2 <- glmer(diagnosis ~ mean_pitch + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_2, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_2[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_2[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_2[N] = c_matrix_test$byClass['Balanced Accuracy']

model_3 <- glmer(diagnosis ~  sd_pitch + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_3, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_3[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_3[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_3[N] = c_matrix_test$byClass['Balanced Accuracy']

model_4 <- glmer(diagnosis ~ median_pitch + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_4, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_4[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_4[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_4[N] = c_matrix_test$byClass['Balanced Accuracy']

model_5 <- glmer(diagnosis ~ min_pitch + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_5, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_5[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_5[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_5[N] = c_matrix_test$byClass['Balanced Accuracy']

model_6 <- glmer(diagnosis ~ max_pitch + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_6, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_6[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_6[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_6[N] = c_matrix_test$byClass['Balanced Accuracy']

model_7 <- glmer(diagnosis ~ iqr + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_7, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_7[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_7[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_7[N] = c_matrix_test$byClass['Balanced Accuracy']

model_8 <- glmer(diagnosis ~ mad + (1 + trial|subject), data_train, family = "binomial") 

pred_perc_test <- inv.logit(predict(model_8, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_8[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_8[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_8[N] = c_matrix_test$byClass['Balanced Accuracy']

model_9 <- glmer(diagnosis ~ CV + (1 + trial|subject), data_train, family = "binomial") 

  pred_perc_test <- inv.logit(predict(model_9, newdata = data_test, allow.new.levels = T)) 
  preds_test <- ifelse(pred_perc_test >= 0.5,1,0)
  preds_test <- as.factor(preds_test)
  
  c_matrix_test <- confusionMatrix(data = preds_test, reference = data_test$diagnosis, positive = '1')
  
  sens_9[N] <- sensitivity(data = preds_test, reference = data_test$diagnosis, positive = "1")
  spec_9[N] <- specificity(data = preds_test, reference = data_test$diagnosis, negative = "0")
  acc_9[N] = c_matrix_test$byClass['Balanced Accuracy']

  N = N + 1
  } 
```


```{r}
#calculate performance measures
#Find the average performance for each performance measure
mean_sen2 <- mean(sens_2)
mean_sen3 <- mean(sens_3)
mean_sen4 <- mean(sens_4)
mean_sen5 <- mean(sens_5)
mean_sen6 <- mean(sens_6)
mean_sen7 <- mean(sens_7)
mean_sen8 <- mean(sens_8)
mean_sen9 <- mean(sens_9)
mean_spe2 <- mean(spec_2)
mean_spe3 <- mean(spec_3)
mean_spe4 <- mean(spec_4)
mean_spe5 <- mean(spec_5)
mean_spe6 <- mean(spec_6)
mean_spe7 <- mean(spec_7)
mean_spe8 <- mean(spec_8)
mean_spe9 <- mean(spec_9)
mean_acc2 <- mean(acc_2)
mean_acc3 <- mean(acc_3)
mean_acc4 <- mean(acc_4)
mean_acc5 <- mean(acc_5)
mean_acc6 <- mean(acc_6)
mean_acc7 <- mean(acc_7)
mean_acc8 <- mean(acc_8)
mean_acc9 <- mean(acc_9)
```

### Question 3 - Which combination of acoustic predictors is best for diagnosing schizophrenia?
Now it's time to go wild! Use all (voice-related) variables and interactions you can think of. Compare models and select the best performing model you can find.
Remember:
- Out-of-sample error crucial to build the best model!
- After choosing the model, send Malte and Riccardo the code of your model
```{r}
#tested a lot of different models in the cross-validation
```

### Question 4: Properly report the results
METHODS SECTION: how did you analyse the data? That is, how did you extract the data, designed the models and compared their performance?

RESULTS SECTION: can you diagnose schizophrenia based on voice? which features are used? Comment on the difference between the different performance measures.

### Bonus question 5
You have some additional bonus data involving speech rate, pauses, etc. Include them in your analysis. Do they improve classification?

### Bonus question 6
Logistic regression is only one of many classification algorithms. Try using others and compare performance. Some examples: Discriminant Function, Random Forest, Support Vector Machine, etc. The package caret provides them.